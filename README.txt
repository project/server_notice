INTRODUCTION
-------------

This module provides a visual cue to let users know which server they are on.
This is implemented through a coloured border around the viewport, and 
additional text if desired.

REQUIREMENTS
-------------

 * colour_field module: https://www.drupal.org/project/color_field

INSTALLATION
-------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
--------------

 * Configure user permissions in Administration » People » Permissions:

    - View Server Notices

      Users with this permission are able to view server notice, 
      which will display on all pages.

 * Visit /admin/config/user-interface/server-notice to add and edit 
 your server notices.

MAINTAINERS
------------

Current maintainer:
 * Rebecca Anderson (beccaneer) - https://www.drupal.org/user/3542843
